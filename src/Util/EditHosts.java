package Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class EditHosts {

    private final String ip = "107.6.175.181", host = "mf.svc.nhl.com";
    private boolean wrongIP = false;

    public boolean hostsFileEdited() {
        Scanner s = null;
        boolean edited = false;
        try {
            if (InetAddress.getByName(new URL("http://" + host).getHost()).getHostAddress().equals(ip)) {
                return true;
            } else {
                File hosts;

                if (System.getProperty("os.name").toLowerCase().contains("win")) {
                    hosts = new File(System.getenv("WINDIR") + "\\system32\\drivers\\etc\\hosts");
                } else {
                    hosts = new File("/etc/hosts");
                }

                s = new Scanner(hosts);
                while (s.hasNext()) {
                    String line = s.nextLine();
                    if (line.startsWith(ip) && line.contains(host)) {
                        edited = true;
                        break;
                    } else if (line.contains(host)) {
                        wrongIP = true;
                        break;
                    }
                }
            }
        } catch (FileNotFoundException | UnknownHostException | MalformedURLException ex) {
            ex.printStackTrace();
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return edited;
    }

    public boolean editHosts() {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            return editWindowsHosts();
        } else {
            return editUnixHosts();
        }
    }

    private boolean editUnixHosts() {
        String p = "echo \'" + Props.getPW() + "\' | sudo -S ", line = "\n" + ip + " " + host;
        Process e;
        try {
            e = new ProcessBuilder(new String[]{"/bin/sh", "-c", p + "-- sh -c \"echo \'" + line + "\' >> /etc/hosts\""}).start();
            e.waitFor();
            return hostsFileEdited();
        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
            
            return false;
        }
    }

    private boolean editWindowsHosts() {
        try {
            String line = "\n" + ip + " " + host;
            Files.write(Paths.get(System.getenv("WINDIR") + "\\system32\\drivers\\etc\\hosts"), line.getBytes(), StandardOpenOption.APPEND);
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean modifyHosts() {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            return modifyWindowsHosts();
        } else {
            return modifyUnixHosts();
        }
    }

    private boolean modifyUnixHosts() {
        String p = "echo \'" + Props.getPW() + "\' | sudo -S ";
        Process m;
        try {
            if (System.getProperty("os.name").toLowerCase().contains("mac")) {
                m = new ProcessBuilder("/bin/sh", "-c", p + "sed -E -i '' \"s/^ *[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+( +" + host + ")/" + ip + "\\1/\" /etc/hosts").start();
            } else {
                m = new ProcessBuilder("/bin/sh", "-c", p + "sed -r -i \"s/^ *[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+( +" + host + ")/" + ip + "\\1/\" /etc/hosts").start();
            }
            m.waitFor();
            return hostsFileEdited();
        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
            
            return false;
        }
    }

    private boolean modifyWindowsHosts() {
        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fw = null;
        boolean modified = false;
        try {
            fr = new FileReader(new File(System.getenv("WINDIR") + "\\system32\\drivers\\etc\\hosts"));
            String s;
            StringBuilder totalStr = new StringBuilder();
            br = new BufferedReader(fr);
            while ((s = br.readLine()) != null) {
                if (s.contains(host)) {
                    s = ip + " " + host;
                }
                totalStr.append(s).append("\r\n");
            }
            fw = new FileWriter(new File(System.getenv("WINDIR") + "\\system32\\drivers\\etc\\hosts"));
            fw.write(totalStr.toString());
            modified = true;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return modified;
    }

    public boolean isWrongIP() {
        return wrongIP;
    }
}
